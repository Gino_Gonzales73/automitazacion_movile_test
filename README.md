
>Este proyecto, tiene el fin de poderse usar para pruebas de dispositivos moviles, apuntando hacia una APK 
precargada. no es nesario tener el APK instalado el proyecto te lo instala por default, solo se debe tener 
las propiedas del dispositivo actualizados en serenity.propierties.

**CONFIGURACION:**

*es importante tener siempre los CAPABILITIES DEL DISPOSITIVO : https://appium.io/docs/en/writing-running-appium/caps/*

1.  Tener acutalizados los capabilities:

     serenity.properties
      --
        -- Abrir CMD
        -- adb devices -- este mostrara el deviceid de su dispositivo
        -- devicename = {se debe ingresar el id de su dispositivo}
        
        Ejemplo: 
        
        C:\Users\everis>adb devices
        List of devices attached
        R5CR70CEB1N     device
        
        appium.deviceName  = R5CR70CEB1N
    

2.  Tener la direccion de al apk NativeDemo.apk ubicado dentro de la carpeta raiz

    serenity.properties
     --
        -- copiar el Path/ruta del apk: NativeDemo.Apk
        -- click derecho -> Copy Path -> Absolute Path
        -- Pegar y ajustar en appium.app
        
        Ejemplo:
        Copy Absolute path    : C:\Users\everis\Desktop\Tunki_Auto\web-test-mobile\NativeDemo.apk
        Ajustar               : /Users/everis/Desktop/Tunki_Auto/web-test-mobile/NativeDemo.apk
        Pegar en la propiedad : appium.app=/Users/everis/Desktop/Tunki_Auto/web-test-mobile/NativeDemo.apk
        
**EJECUCION:**

*para poder correr la prueba elaborada dentro del proyecto.*

1.  Desarrollado con Appium 1.21.0
2.  Desarrollado con gradle 6.9
3.  Desarrollado con JDK 8

        -- Ejecutar en la terminal de manera directa
        
        -- gradle clean test -Dcucumber.options="--tags @mobile_test"
        
        
        
        -- Ejecutar Click derecho -> RUN
        
        *** java/com/baufest/RunnerCredentialsSubLogin.java
        *** java/com/baufest/RunnerNavigateWebView.java
        *** java/com/baufest/RunnerRegisterSubForms.java
        
