@mobile_test
Feature: Reealizar un registro y loguearnos con las credenciales ingresadas

  Scenario Outline: validar registro y logueo
    Given soy un usuario nuevo y quiera registrarme
    When completo el campo usuario con <mail>
    And completo el campo password con <password>
    And confirmo mis datos del registro
    #Se realizara un registro exitoso
    And me direcciono a la venta Login
    And confirmo mis datos del login
    Then se mostrara un mensaje de Exito
    Examples:
      | mail             | password |
      | test01@gmail.com | 12345678 |
