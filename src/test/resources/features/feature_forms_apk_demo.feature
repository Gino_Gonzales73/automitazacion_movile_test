@mobile_test
Feature: Ingresar datos al formulario

  Scenario Outline: completar el formulario con mis datos
    Given que tenga mis datos vacios
    When escribo <anything> en el campo de entrada
    And pondremos en <status> el interrupotr
    And seleccionamos una herramienta de automatizacion
    And confirmo con el boton Active
    Then se mostrara un mensaje de activacion
    Examples:
      | anything                    | status |
      | me gusta la musica          | off    |
      | me gusta los juegos de mesa | on     |