package com.baufest.page;

import io.appium.java_client.pagefactory.AndroidFindBy;
import net.serenitybdd.core.pages.PageObject;


import net.serenitybdd.core.pages.WebElementFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PageLogin extends PageObject {

    private Logger log= LoggerFactory.getLogger(this.getClass());

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Login']")
    WebElementFacade subMenuLogin;

    @AndroidFindBy(xpath = "//android.widget.TextView[@index='0'][@text='Login']")
    WebElementFacade windowOneLogin;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Sign up']")
    WebElementFacade windowTwoSignUp;

    @AndroidFindBy(accessibility = "input-email")
    WebElementFacade inputTextMail;

    @AndroidFindBy(accessibility = "input-password")
    WebElementFacade inputTextPassword;

    @AndroidFindBy(accessibility = "input-repeat-password")
    WebElementFacade inputTextRepeatPassword;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='SIGN UP']")
    WebElementFacade buttonSendRegister;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='LOGIN']")
    WebElementFacade buttonSendLogin;

    @AndroidFindBy(id = "android:id/button1")
    WebElementFacade buttonSuccessfulRegister;

    @AndroidFindBy(id = "android:id/message")
    WebElementFacade getTextSuccessfulLogin;


    //INGRESANDO AL SUBMENU LOGIN
    public void clickLoginOptions(){
        element(subMenuLogin).waitUntilPresent().click();
    }

    // VENTANAS DE LOGIN
    public void clickWindowOneLogin(){
        element(windowOneLogin).waitUntilPresent().click();
    }

    public void clickWindowTwoSignUp(){
        element(windowTwoSignUp).waitUntilPresent().click();
    }

    //REGISTRO Y LOGEO
    public void sendTextMail(String mail){
        element(inputTextMail).waitUntilPresent().sendKeys(mail);
    }

    public void sendTextPassword(String password){
        element(inputTextPassword).waitUntilPresent().sendKeys(password);
    }

    public void sendTextRepeatPassword(String password){
        element(inputTextRepeatPassword).waitUntilPresent().sendKeys(password);
    }
    public void clickConfirmCredentialsRegister(){
        element(buttonSendRegister).waitUntilPresent().click();
        log.info("CONFIRMAMOS TODAS LOS DATOS INGRESADOS");
    }

    public void clickConfirmCredentialsLogin(){
        element(buttonSendLogin).waitUntilPresent().click();
    }

    //ALERTAS MENSAJES

    public void alertClickAcceptRegister(){
        element(buttonSuccessfulRegister).waitUntilPresent().click();
        log.info("SALDREMOS DE LA ALERTA");
    }

    public String alertGetTextMessageLogin(){
        return  element(getTextSuccessfulLogin).waitUntilPresent().getText();
    }
}
