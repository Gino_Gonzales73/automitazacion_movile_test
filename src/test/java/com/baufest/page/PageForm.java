package com.baufest.page;

import io.appium.java_client.pagefactory.AndroidFindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PageForm extends PageObject {
    private Logger log= LoggerFactory.getLogger(this.getClass());

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Forms']")
    WebElementFacade subMenuForms;

    @AndroidFindBy(accessibility = "text-input")
    WebElementFacade inputTextTypeSomething;

    @AndroidFindBy(accessibility = "switch")
    WebElementFacade switchTurnForms;
    @AndroidFindBy(accessibility = "switch-text")
    WebElementFacade getTextSwitchTurnForms;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Select a value here']")
    WebElementFacade comboBoxValueTool;
    @AndroidFindBy(xpath = "//android.widget.CheckedTextView[@text='Appium is awesome']")
    WebElementFacade comboBoxOptionTwo;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Active']")
    WebElementFacade buttonActiveForm;

    @AndroidFindBy(id = "android:id/message")
    WebElementFacade getTextSuccessfulForm;

    //INGRESANDO AL SUBMENU FORMS
    public void clickFormsOptions(){
        element(subMenuForms).waitUntilPresent().click();
        log.info("INGRESANDO AL SUBMENUO FORMS");
    }

    //COMPLETAR FORMULARIO
    public void sendTextTypeSomething(String anything){
        element(inputTextTypeSomething).waitUntilPresent().sendKeys(anything);
    }

    public void clickSwitchTurnForm(String status){
        String statusDefault = getTextSwitchTurnForms.getText().substring(25,27);
        if(!statusDefault.equals(status.toUpperCase())){
            element(switchTurnForms).waitUntilPresent().click();
            log.info("EL BOTON YA SE ENCUENTRA APAGADO -> OFF");
        }else {
            log.info("EL BOTON YA SE ENCUENTRA ENCENDIDO -> ON");
        }
    }


    //ACA SE PUEDE HACER UN METODO PARA QUE SE PUEDA SELECIONAR POR POSICION
    public void visualizeComboBoxForms(){
        element(comboBoxValueTool).waitUntilPresent().click();
        element(comboBoxOptionTwo).waitUntilPresent().click();
    }

    public void clickConfirmForms(){
        element(buttonActiveForm).waitUntilPresent().click();
    }
    //ALERTAS MENSAJES
    public String alertGetTextMessageActive(){
        return element(getTextSuccessfulForm).waitUntilPresent().getText();
    }

}
