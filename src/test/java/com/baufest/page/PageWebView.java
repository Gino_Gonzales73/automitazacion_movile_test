package com.baufest.page;
import io.appium.java_client.*;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.touch.offset.PointOption;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Screenshots;
import net.thucydides.core.webdriver.WebDriverFacade;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PageWebView extends PageObject  {

    private Logger log= LoggerFactory.getLogger(this.getClass());

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='WebView']")
    WebElementFacade subMenuWebView;

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[1]/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.widget.Button[2]")
    WebElementFacade buttonSearchNavigate;

    @AndroidFindBy(xpath = "//android.widget.EditText[@index='1']")
    WebElementFacade inputSearchNavigate;

    @AndroidFindBy(accessibility = "The Browser Object")
    WebElementFacade buttonBrowserObject;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='Navigation bar toggle']")
    WebElementFacade buttonTabWebView;

    @AndroidFindBy(xpath = "//android.view.View[@index='3']")
    WebElementFacade elementsTabWebView;


    //INGRESANDO AL SUBMENU WEB VIEW
    public void clickWebViewOptions(){
        element(subMenuWebView).waitUntilPresent().click();
    }

    public void clickSearchNavigate(){
        element(buttonSearchNavigate).waitUntilPresent().click();
    }

    public void sendTextForSearch(String word){
        element(inputSearchNavigate).waitUntilPresent().sendKeys(word);
        element(buttonBrowserObject).waitUntilPresent().click();
    }


    @Screenshots(forEachAction = true)
    public void interactionTabWebView(){
        Integer positionX = 120; // VALOR INGRESAR DE X DE LA PALABRA DOCS
        Integer positionY = 417; // VALOR INGRESAR DE Y DE LA PALABRA DOCS
        try{
            for(int i = 1; i<6; i++){
                Thread.sleep(1500);
                element(buttonTabWebView).waitUntilPresent().click();
                Integer positionYadd = positionY;
                touch(positionX,positionYadd);
                positionY = positionYadd + 108;
            }
        }catch (Exception e){
            log.error("SE PRESENTA UN ERROR EN LA SELCCION DE LOS TABS -> METHOD interactionTabWebView ");
        }

    }

    public void touch(Integer x, Integer y){
        WebDriver facade = getDriver();
        WebDriver driver= ((WebDriverFacade) facade).getProxiedDriver();
        TouchAction actions = new TouchAction((MobileDriver)driver) ;
        actions.tap(new PointOption().withCoordinates(x,y)).perform();
    }


}
