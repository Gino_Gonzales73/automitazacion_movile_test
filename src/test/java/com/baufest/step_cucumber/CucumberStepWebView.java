package com.baufest.step_cucumber;

import com.baufest.step_serenity.SerenityStepsWebView;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class CucumberStepWebView {

    @Steps
    SerenityStepsWebView serenityStepsWebView;

    @Given("^que ingreso a la opcion WebView$")
    public void demo_start_session_webview(){
        serenityStepsWebView.demo_start_session_webview();
    }

    @When("^busco en el navegador la palabra ([^\"]*)$")
    public void demo_search_text_webview(String word){
        serenityStepsWebView.demo_search_text_webview(word);
    }

    @And("^navego por todo los tabs$")
    public void demo_navigate_tabs_webview(){
        serenityStepsWebView.demo_navigate_tabs_webview();
    }

    @Then("^todo estara correcto$")
    public void demo_validate_message(){
        serenityStepsWebView.demo_validate_message();
    }
}
