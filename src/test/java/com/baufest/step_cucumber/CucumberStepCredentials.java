package com.baufest.step_cucumber;

import com.baufest.step_serenity.SerenityStepsCredential;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class CucumberStepCredentials {

    @Steps
    SerenityStepsCredential serenityStepCredentials;


    @Given("^soy un usuario nuevo y quiera registrarme$")
    public void demo_start_app_session(){
        serenityStepCredentials.demo_start_app_session();
    }

    @When("^completo el campo usuario con ([^\"]*)$")
    public void demo_send_text_mail(String mail){
        serenityStepCredentials.demo_send_text_mail(mail);
    }

    @And("^completo el campo password con ([^\"]*)$")
    public void demo_send_text_password(String password){
        serenityStepCredentials.demo_send_text_password(password);
    }

    @And("^confirmo mis datos del registro$")
    public void demo_confirm_credentials_register(){
        serenityStepCredentials.demo_confirm_credentials();
    }

    @And("^me direcciono a la venta Login$")
    public void demo_position_window_login(){
        serenityStepCredentials.demo_position_window_login();
    }

    @And("^confirmo mis datos del login$")
    public void demo_confirm_credentials_login(){
        serenityStepCredentials.demo_confirm_credentials_login();
    }

    @Then("^se mostrara un mensaje de Exito$")
    public void demo_validate_message(){
        serenityStepCredentials.demo_validate_message();
    }


}
