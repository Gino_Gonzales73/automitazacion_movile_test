package com.baufest.step_cucumber;

import com.baufest.step_serenity.SerenityStepsForm;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class CucumberStepForm {

    @Steps
    SerenityStepsForm serenityStepsForm;

    @Given("^que tenga mis datos vacios$")
    public void demo_start_session_form(){
        serenityStepsForm.demo_start_session_form();
    }

    @When("^escribo ([^\"]*) en el campo de entrada$")
    public void demo_send_text_anything_form(String anything){
        serenityStepsForm.demo_send_text_anything_form(anything);
    }

    @And("^pondremos en ([^\"]*) el interrupotr$")
    public void demo_click_status_form(String status){
        serenityStepsForm.demo_click_status_form(status);
    }

    @And("^seleccionamos una herramienta de automatizacion$")
    public void demo_combo_box_form(){
        serenityStepsForm.demo_combo_box_form();
    }

    @And("^confirmo con el boton Active$")
    public void demo_confirm_button_active_form(){
        serenityStepsForm.demo_confirm_button_active_form();
    }

    @Then("^se mostrara un mensaje de activacion$")
    public void demo_validate_message_active_form(){
        serenityStepsForm.demo_validate_message_active_form();
    }
}
