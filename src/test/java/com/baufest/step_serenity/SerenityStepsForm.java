package com.baufest.step_serenity;

import com.baufest.page.PageForm;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Assert;

public class SerenityStepsForm extends ScenarioSteps {

    PageForm pageForm;

    @Step("Se inicia session en el aplicativo en el Sub Menu Forms")
    public void demo_start_session_form() {
        pageForm.clickFormsOptions();
    }

    @Step("Se ingresara el texto {0} en el campo Type Something")
    public void demo_send_text_anything_form(String anything) {
        pageForm.sendTextTypeSomething(anything);
    }

    @Step("Se dejara en estado {0} el interruptor")
    public void demo_click_status_form(String status) {
        pageForm.clickSwitchTurnForm(status);
    }

    @Step("Se seleccionara la herramienta de automatizacion")
    public void demo_combo_box_form() {
        pageForm.visualizeComboBoxForms();
    }

    @Step("Se confirmara el formulario con el boton Active")
    public void demo_confirm_button_active_form() {
        pageForm.clickConfirmForms();
    }

    @Step("Se visualizara mensaje de boton esta activo")
    public void demo_validate_message_active_form(){
        Assert.assertEquals(pageForm.alertGetTextMessageActive(),"This button is active");
    }
}
