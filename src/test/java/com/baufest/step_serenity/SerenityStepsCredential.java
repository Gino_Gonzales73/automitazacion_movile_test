package com.baufest.step_serenity;

import com.baufest.page.PageLogin;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Assert;

public class SerenityStepsCredential extends ScenarioSteps {

    PageLogin pageLogin;

    @Step("Se inicia session en el aplicativo en el Sub Menu Login")
    public void demo_start_app_session(){
        pageLogin.clickLoginOptions();
        pageLogin.clickWindowTwoSignUp();
    }

    @Step("Se ingresara el valor {0} en el campo Email")
    public void demo_send_text_mail(String mail){
        pageLogin.sendTextMail(mail);
    }

    @Step("Se ingresara el valor {0} en el campo Password")
    public void demo_send_text_password(String password){
        pageLogin.sendTextPassword(password);
        pageLogin.sendTextRepeatPassword(password);
    }

    @Step("Se confirmara los datos ingresados")
    public void demo_confirm_credentials(){
        pageLogin.clickConfirmCredentialsRegister();
        pageLogin.alertClickAcceptRegister();
    }

    @Step("Me posicionare en la Ventana de Login")
    public void demo_position_window_login(){
        pageLogin.clickWindowOneLogin();
    }

    @Step("Confirmare el Login con mis credenciales")
    public void demo_confirm_credentials_login(){
        pageLogin.clickConfirmCredentialsLogin();
    }

    @Step("Se validara el mensaje obtenido")
    public void demo_validate_message(){
        Assert.assertEquals(pageLogin.alertGetTextMessageLogin(),"You are logged in!");
    }

}
