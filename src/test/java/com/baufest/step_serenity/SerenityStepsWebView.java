package com.baufest.step_serenity;

import com.baufest.page.PageWebView;
import io.appium.java_client.AppiumDriver;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class SerenityStepsWebView extends ScenarioSteps {

    PageWebView pageWebView;

    @Step("Se inicia session en el aplicativo en el Sub Menu WebView")
    public void demo_start_session_webview() {
        pageWebView.clickWebViewOptions();
    }

    @Step("Buscare la palabra {0} en el navegador")
    public void demo_search_text_webview(String word) {
        pageWebView.clickSearchNavigate();
        pageWebView.sendTextForSearch(word);
    }

    @Step("Interactuare en todo las opciones del TAB")
    public void demo_navigate_tabs_webview() {
        pageWebView.interactionTabWebView();
    }

    @Step("Esperare un mensaje de validacion")
    public void demo_validate_message() {
    }
}
