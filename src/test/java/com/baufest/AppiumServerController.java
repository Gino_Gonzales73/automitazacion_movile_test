package com.baufest;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

import static io.appium.java_client.service.local.AppiumDriverLocalService.buildService;

public final class AppiumServerController {

    private final static AppiumDriverLocalService service;

    static {
        System.out.println("Print Default Data");
        System.out.println(Property.APPIUM_HOST +"    "+ Property.APPIUM_PORT + "    " + GeneralServerFlag.LOG_LEVEL + "   " + Property.IMPLICIT_WAIT_TIME );

        service = buildService(new AppiumServiceBuilder()
                .withIPAddress("0.0.0.0")
                .usingPort(Integer.parseInt("4727"))
                //.withAppiumJS(new File("/usr/local/lib/node_modules/appium/build/lib/main.js"))
                .withArgument(GeneralServerFlag.LOG_LEVEL, "info"));
    }

    public static void startAppiumServer() {
        try{
            service.start();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void stopAppiumServer() {
        try{
            if (service.isRunning()) {
                service.stop();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}